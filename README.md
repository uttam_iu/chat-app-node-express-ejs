
<p align="center">
  <h3 align="center">Node.js Chat Application in Express.js, MongoDB & EJS template engine</a></h3>

<!-- HOW TO RUN -->

## How to run

Please follow the below instructions to run this project in your machine:

1. Clone this repository
   ```sh
   git clone https://uttam_iu@bitbucket.org/uttam_iu/chat-app-node-express-ejs.git
   ```
2. Run npm install
3. Then rename the .env.example file to ".env" and change values as per your need
4. Finally run the application in development using below command -
   ```sh
   npm start
   ```
   or
   You can run it in production mode by -
   ```sh
   npm run prod
   ```